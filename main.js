const express = require('express');
const puppeteer = require('puppeteer');

const app = express();
const port = 3000;

app.get('/', async (request, response) => {
  try {
    const data = await scrapeInstagramReel();
    response.status(200).json(data);
  } catch (error) {
    console.error(error);
    response.status(500).json({
      message: 'Server error occurred',
    });
  }
});

app.listen(port, () => {
  console.log(`Started`);
});

async function scrapeInstagramReel() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  try {
    await page.goto('https://www.instagram.com/reel/CLeHQPhpvWV/');

    // Wait for the video element to be present, adjust as needed
    await page.waitForSelector('video', { visible: true });

    const videoSrc = await page.$eval('video', (video) => video.src);

    return {
      videoSrc: videoSrc || '',
    };
  } catch (error) {
    console.error('Scraping error:', error);
    throw new Error('Web scraping failed');
  } finally {
    await browser.close();
  }
}
